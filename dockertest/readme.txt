see: https://www.howtoforge.com/tutorial/how-to-create-docker-images-with-dockerfile/

----
docker without sudo, see https://askubuntu.com/questions/477551/how-can-i-use-docker-without-sudo
sudo gpasswd -a $USER docker
newgrp docker
----

in directory dockertest:
docker build -t nginx_image .

docker images
-> nginx_image must be there

docker run -d -v $pwd/dockertest/webroot:/var/www/html -p 33333:80 --name myimagename nginx_image

echo '<?php phpinfo(); ?>' > webroot/info.php

in browser:
http://yourserver:33333/info.php

interactive bash:
docker run -t -i -v $pwd/dockertest/webroot:/var/www/html -p 33333:80 --name myimagename nginx_image /bin/bash

docker stop myimagename 
docker rm myimagename

